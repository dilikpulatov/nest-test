STACK=test_nest

build:
	docker compose up --build -d

pull:
	git checkout main && git pull origin main

commit:
	git checkout main && git add . && git commit -m "$m"

push:
	git checkout main && git push origin main
