import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { AuthService } from "@/modules/user/auth.service";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly authService: AuthService
  ) {
  }

  async use(req: Request & {user: any}, res: Response, next: NextFunction) {
    req.user = await this.authService.getUserByToken(req);
    next();
  }
}
