import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthService } from "@/modules/user/auth.service";

@Injectable()
export class JwtAuthGuard implements CanActivate{
  constructor(
    private readonly authService: AuthService
  ) {
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.authService.verifyToken(request);
  }
}
