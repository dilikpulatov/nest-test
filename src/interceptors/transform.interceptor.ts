import { Injectable, NestInterceptor, ExecutionContext, CallHandler, HttpStatus } from "@nestjs/common";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IServiceResponse } from "@/types/global/general";
import { generateResponse } from "@/helpers/helper-functions";

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, IServiceResponse<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<IServiceResponse<T>> {
    const response = context.switchToHttp().getResponse();
    const status = !!response.statusCode ? response.statusCode : HttpStatus.OK;
    return next.handle().pipe(map(data => {
      return generateResponse(data,[],status);
    }));
  }
}
