import { HttpStatus } from '@nestjs/common';
import { ICrudListResponse, IListPagination, IServiceResponse } from '@/types/global/general';

export const generateResponse = <T>(data: T = null, messages: string[] = [], statusCode: HttpStatus =  HttpStatus.OK): IServiceResponse<T> => {
  return {
    data,
    messages,
    statusCode,
    time: new Date()
  }
}

export const generateCrudListResponse = <T>(list: Array<T> = [], pagination: IListPagination): ICrudListResponse<T> => {
  return {
    list,
    pagination
  }
}

export const generationPaginate = (total_count: number, page: number, limit: number): IListPagination => {
  return {
    total_count: total_count,
    total_pages: Math.ceil(total_count / limit),
    current_page: page,
    limit
  }
}
