import {HttpStatus} from '@nestjs/common';

export interface IServiceResponse<T> {
  data: T | any;
  messages: Array<string>;
  statusCode: HttpStatus;
  time: Date | string;
}

export interface ICrudListResponse<T> {
  list: Array<T>;
  pagination: IListPagination;
}

export interface IListPagination {
  total_count: number;
  total_pages: number;
  current_page: number;
  limit: number;
}
