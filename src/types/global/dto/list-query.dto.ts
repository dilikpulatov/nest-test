import {IsInt, IsOptional, Min} from 'class-validator';
import {Type} from 'class-transformer';

export class ListQueryDto {
  @IsInt()
  @Min(1)
  @IsOptional()
  @Type(() => Number)
  page = 1;

  @IsInt()
  @Min(1)
  @IsOptional()
  @Type(() => Number)
  limit = 10;
}
