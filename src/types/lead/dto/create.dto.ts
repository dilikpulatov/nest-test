import { IsDefined, IsEmail, IsInt, IsOptional, IsString, Matches } from "class-validator";

export class CreateDto{
  @IsInt()
  @IsOptional()
  agent_id?: number;

  @Matches(/^998[0-9]{9}$/, {message: _ => `Неправильный формат телефон номера.`})
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  phone: string;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsString()
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  name: string;
}
