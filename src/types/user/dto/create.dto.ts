import { IsDefined, IsEmail, IsEnum, IsOptional, IsString, Length, Matches } from "class-validator";
import { UserType } from "@/types/user/general";

export class CreateDto{
  @IsEnum(UserType, { each: true })
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  role: number;

  @Matches(/^998[0-9]{9}$/, {message: _ => `Неправильный формат телефон номера.`})
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  phone: string;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsString()
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  first_name: string;

  @IsString()
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  last_name: string;

  @IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  @Length(6, 32,{ message: _ => `Длина пароля должна быть не менее 6 символов.` })
  password: string;
}
