export enum UserType {
  ADMINISTRATOR = 1,
  MANAGER = 2,
  AGENT = 3
}
