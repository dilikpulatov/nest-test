
export interface IProfile {
  id: number;
  role: number;
  phone: string;
  email: string;
  first_name: string;
  last_name: string;
}

export interface ILoginResponse {
  access_token: string;
}

export interface IVerifyTokenResponse {
  is_verify: boolean,
  profile: IProfile
}
