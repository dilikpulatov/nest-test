import { IsString, IsDefined, Matches, Length } from "class-validator";

export class LoginDto {
  @Matches(/^998[0-9]{9}$/, {message: _ => `Неправильный формат телефон номера.`})
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  phone: string;

  @IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  @Length(6, 32,{ message: _ => `Длина пароля должна быть не менее 6 символов.` })
  password: string;
}
