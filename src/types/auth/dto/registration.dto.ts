import { IsString, IsDefined, IsEnum } from "class-validator";
import { UserType } from "@/types/user/general";
import { LoginDto } from "@/types/auth/dto/login.dto";

export class RegistrationDto extends LoginDto{
  @IsEnum(UserType, { each: true })
  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  role: number;

  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  @IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
  first_name: string;

  @IsDefined({ message: args => `Значение "${args.property}" не может быть неопределенным.` })
  @IsString({ message: args => `Значение "${args.property}" должен быть строкой.` })
  last_name: string;
}
