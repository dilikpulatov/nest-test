import { SetMetadata } from '@nestjs/common';
import { UserType } from "@/types/user/general";

export const Roles = (roles: UserType[]) => SetMetadata('roles', roles);
