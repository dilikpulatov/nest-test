import { Injectable, NotFoundException } from "@nestjs/common";
import { ListQueryDto } from "@/types/global/dto/list-query.dto";
import { ICrudListResponse } from "@/types/global/general";
import { Leads } from "@prisma/client";
import { generateCrudListResponse, generationPaginate } from "@/helpers/helper-functions";
import { CreateDto } from "@/types/lead/dto/create.dto";
import { UpdateDto } from "@/types/lead/dto/update.dto";
import { PrismaService } from "@/modules/prisma/prisma.service";
import { UserService } from "@/modules/user/user.service";
import { IProfile } from "@/types/auth/general";
import { UserType } from "@/types/user/general";

@Injectable()
export class LeadService {
  constructor(
    private prisma: PrismaService,
    private readonly userService: UserService
  ) {}

  /* CRUD */
  async getAll(query: ListQueryDto, user: IProfile): Promise<ICrudListResponse<Leads>> {
    const totalCount = await this.prisma.leads.count();
    let userWhere = {}
    if (user.role === UserType.AGENT ) {
      userWhere = {
        agents: {
          some: {
            agent_id: user.id
          }
        }
      }
    } else if (user.role === UserType.MANAGER) {
      userWhere = {
        NOT: {
          agents: {
            none: {}
          }
        }
      }
    }

    let result = await this.prisma.leads.findMany({
      where: userWhere,
      select: {
        id: true,
        name: true,
        phone: user.role !== UserType.AGENT,
        email: user.role !== UserType.AGENT,
        agents: {
          include: { agent: true }
        }
      },
      skip: ((query.page - 1) * query.limit),
      take: query.limit
    });

    return generateCrudListResponse(result.map((item) => {
      return { ...item, agents: item.agents.map((x) => ({
        id: x.agent.id,
        phone: x.agent.phone,
        first_name: x.agent.first_name,
        last_name: x.agent.last_name,
      })) }
    }), generationPaginate(totalCount, query.page, query.limit));
  }

  async getOne(id: number, user: IProfile): Promise<Leads> {
    let userWhere = {};
    if (user.role === UserType.AGENT ) {
      userWhere = {
        agents: {
          some: {
            agent_id: user.id
          }
        }
      }
    } else if (user.role === UserType.MANAGER) {
      userWhere = {
        NOT: {
          agents: {
            none: {}
          }
        }
      }
    }

    const result = await this.prisma.leads.findFirst({
      select: {
        id: true,
        name: true,
        phone: user.role !== UserType.AGENT,
        email: user.role !== UserType.AGENT,
        agents: {
          include: { agent: true }
        }
      },
      where: {
        id,
        ...userWhere
      }
    });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async create(data: CreateDto): Promise<Leads> {
    try {
      const lead = await this.prisma.leads.create({
        data: {
          name: data.name,
          phone: data.phone,
          email: data.email ? data.email : null,
        },
        include: {agents: true}
      });
      if (data.agent_id) {
        const agent = await this.userService.getAgentById(data.agent_id);
        if (!!agent) {
          await this.prisma.leadsOnUsers.create({
            data: {
              lead_id: lead.id,
              agent_id: data.agent_id
            }
          });
        }
      }
      return lead;
    } catch (e) {
      throw e;
    }
  }

  async update(id: number, data: UpdateDto): Promise<Leads> {
    try {
      return this.prisma.leads.update({
        where: { id },
        data
      });
    } catch (e) {
      throw e;
    }
  }

  delete(id: number): Promise<Leads> {
    try {
      return this.prisma.leads.delete({
        where: { id }
      });
    } catch (e) {
      throw e;
    }
  }
}
