import { Module } from '@nestjs/common';
import { LeadService } from './lead.service';
import { LeadController } from './lead.controller';
import { UserModule } from "@/modules/user/user.module";
import { PrismaModule } from "@/modules/prisma/prisma.module";

@Module({
  imports: [UserModule, PrismaModule],
  providers: [LeadService],
  controllers: [LeadController]
})
export class LeadModule {}
