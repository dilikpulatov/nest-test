import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query, Req,
  UseGuards
} from "@nestjs/common";
import { LeadService } from "@/modules/lead/lead.service";
import { JwtAuthGuard } from "@/guards/jwt-auth.guard";
import { ListQueryDto } from "@/types/global/dto/list-query.dto";
import { GetOneDto } from "@/types/global/dto/get-one.dto";
import { CreateDto } from "@/types/lead/dto/create.dto";
import { UpdateDto } from "@/types/lead/dto/update.dto";
import { RolesGuard } from "@/guards/roles.guard";
import { Roles } from "@/decorators/roles.decorator";
import { UserType } from "@/types/user/general";

@UseGuards(RolesGuard)
@UseGuards(JwtAuthGuard)
@Controller('lead')
export class LeadController {
  constructor(
    private readonly leadService: LeadService
  ) {}

  @Get('')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR, UserType.MANAGER, UserType.AGENT])
  getAll(@Query() query: ListQueryDto, @Req() request) {
    return this.leadService.getAll(query, request.user);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR, UserType.MANAGER, UserType.AGENT])
  getOne(@Param() params: GetOneDto, @Req() request) {
    return this.leadService.getOne(params.id, request.user);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Roles([UserType.ADMINISTRATOR])
  create(@Body() data: CreateDto) {
    return this.leadService.create(data);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  update(@Param() params: GetOneDto, @Body() data: UpdateDto) {
    return this.leadService.update(params.id, data);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  delete(@Param() params: GetOneDto) {
    return this.leadService.delete(params.id);
  }
}
