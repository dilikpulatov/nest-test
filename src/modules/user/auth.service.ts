import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { UserService } from "@/modules/user/user.service";
import { RegistrationDto } from "@/types/auth/dto/registration.dto";
import { Users } from "@prisma/client";
import { JwtService } from "@nestjs/jwt";
import { LoginDto } from "@/types/auth/dto/login.dto";
import { ILoginResponse, IProfile } from "@/types/auth/general";
import { SessionService } from "@/modules/user/session.service";

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly sessionService: SessionService,
    private readonly jwtService: JwtService
  ) {}

  async login(data: LoginDto): Promise<IProfile>{
    const user = await this.userService.findByPhone(data.phone);
    if (!user) {
      throw new UnauthorizedException(`Пользователь с таким телефон номером "${data.phone}" не найдно.`);
    }
    const isCorrectPassword = await this.userService.isCorrectPassword(data.password, user.password);
    if (!isCorrectPassword) {
      throw new UnauthorizedException(`Некорректный телефон номером или пароль.`);
    }
    return {
      id: user.id,
      role: user.role,
      phone: user.phone,
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name
    };
  }

  async generateToken(payload: IProfile): Promise<ILoginResponse> {
    const token = await this.jwtService.signAsync(payload);
    const userSession = await this.sessionService.addNewUserToken(payload.id, token);
    return {
      access_token: userSession.token
    };
  }

  async registrationNewUser(data: RegistrationDto): Promise<Users> {
    const hasUser = await this.userService.findByPhone(data.phone);
    if (!!hasUser) {
      throw new BadRequestException(`Пользователь с таким телефон номером "${data.phone}" существует.`);
    } else {
      return this.userService.createNewUser(data);
    }
  }

  async getUserByToken(request): Promise<IProfile | null> {
    if (typeof request.headers.authorization === 'undefined') {
      return null;
    }
    const token: string = request.headers.authorization.toString().replace('Bearer ', '');
    let profile: IProfile = null;
    await this.jwtService.verifyAsync(token).then((res: any) => {
      profile = res
    }).catch(_ => {
      throw new UnauthorizedException();
    });
    return profile
  }

  async verifyToken(request): Promise<boolean> {
    const token: string = request.headers.authorization.toString().replace('Bearer ', '');
    const userSession = await this.sessionService.checkSessionByToken(token);
    if (typeof userSession.user_id !== 'undefined' && userSession.user_id === request.user.id) {
      return true;
    } else {
      throw new UnauthorizedException();
    }
  }

  async getUserRoleByToken(request): Promise<number> {
    if (typeof request.headers.authorization === 'undefined') {
      throw new UnauthorizedException();
    }
    const token: string = request.headers.authorization.toString().replace('Bearer ', '');
    let role: number = null;
    await this.jwtService.verifyAsync(token).then((profile: IProfile) => {
      role = profile.role
    }).catch(_ => {});
    if (!!role) {
      return role;
    } else {
      throw new UnauthorizedException();
    }
  }
}
