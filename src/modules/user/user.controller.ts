import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseGuards
} from "@nestjs/common";
import { UserService } from "@/modules/user/user.service";
import { GetOneDto } from "@/types/global/dto/get-one.dto";
import { CreateDto } from "@/types/user/dto/create.dto";
import { UpdateDto } from "@/types/user/dto/update.dto";
import { ListQueryDto } from "@/types/global/dto/list-query.dto";
import { JwtAuthGuard } from "@/guards/jwt-auth.guard";
import { RolesGuard } from "@/guards/roles.guard";
import { Roles } from "@/decorators/roles.decorator";
import { UserType } from "@/types/user/general";

@UseGuards(RolesGuard)
@UseGuards(JwtAuthGuard)
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService
  ) {
  }

  @Get('')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  getAll(@Query() query: ListQueryDto) {
    return this.userService.getAll(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  getOne(@Param() params: GetOneDto) {
    return this.userService.getOne(params.id);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Roles([UserType.ADMINISTRATOR])
  create(@Body() data: CreateDto) {
    return this.userService.create(data);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  update(@Param() params: GetOneDto, @Body() data: UpdateDto) {
    return this.userService.update(params.id, data);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @Roles([UserType.ADMINISTRATOR])
  delete(@Param() params: GetOneDto) {
    return this.userService.delete(params.id);
  }
}
