import { Body, Controller, HttpCode, HttpStatus, Post } from "@nestjs/common";
import { RegistrationDto } from "@/types/auth/dto/registration.dto";
import { LoginDto } from "@/types/auth/dto/login.dto";
import { AuthService } from "@/modules/user/auth.service";
import { ILoginResponse } from "@/types/auth/general";

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService
  ) {
  }

  @Post('registration')
  @HttpCode(HttpStatus.CREATED)
  registrationNewUser(@Body() data: RegistrationDto) {
    return this.authService.registrationNewUser(data);
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  async loginUser(@Body() data: LoginDto): Promise<ILoginResponse> {
    const payload = await this.authService.login(data);
    return this.authService.generateToken(payload);
  }
}
