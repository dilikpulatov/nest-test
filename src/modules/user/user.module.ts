import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { SessionService } from './session.service';
import { PrismaModule } from "@/modules/prisma/prisma.module";
import { AuthService } from "@/modules/user/auth.service";
import { JwtStrategy } from "@/modules/user/strategies/jwt.strategy";
import { AuthController } from "@/modules/user/auth.controller";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { ConfigModule, ConfigService } from "@nestjs/config";

@Module({
  providers: [UserService, SessionService, AuthService, JwtStrategy],
  imports: [
    PrismaModule,
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET_KEY'),
        signOptions: { expiresIn: '3d' },
      }),
      inject: [ConfigService],
    }),
  ],
  exports: [UserService, SessionService, AuthService],
  controllers: [UserController, AuthController]
})
export class UserModule {}
