import { Injectable } from '@nestjs/common';
import { PrismaService } from "@/modules/prisma/prisma.service";
import { UsersSession } from "@prisma/client";

@Injectable()
export class SessionService {
  constructor(
    private readonly prisma: PrismaService
  ) {
  }

  async addNewUserToken(user_id: number, token: string): Promise<UsersSession> {
    try {
      return this.prisma.usersSession.create({
        data: {user_id, token}
      });
    } catch (e) {
      throw e;
    }
  }

  async checkSessionByToken(token: string): Promise<UsersSession> {
    try {
      return this.prisma.usersSession.findUnique({
        where: {token}
      });
    } catch (e) {
      throw e;
    }
  }
}
