import { Injectable, NotFoundException } from "@nestjs/common";
import { RegistrationDto } from "@/types/auth/dto/registration.dto";
import { Users } from "@prisma/client";
import { PrismaService } from "@/modules/prisma/prisma.service";
import { compare, genSalt, hash } from "bcryptjs";
import { ListQueryDto } from "@/types/global/dto/list-query.dto";
import { ICrudListResponse } from "@/types/global/general";
import { generateCrudListResponse, generationPaginate } from "@/helpers/helper-functions";
import { CreateDto } from "@/types/user/dto/create.dto";
import { UpdateDto } from "@/types/user/dto/update.dto";
import { UserType } from "@/types/user/general";

@Injectable()
export class UserService {
  constructor(
    private prisma: PrismaService
  ) {}

  async createNewUser(data: RegistrationDto): Promise<Users> {
    try {
      data.password = await this.generatePassword(data.password);
      return this.prisma.users.create({data});
    } catch (e) {
      throw e;
    }
  }

  async findByPhone(phone: string): Promise<Users> {
    return this.prisma.users.findUnique({
      where: {
        phone
      }
    });
  }

  async findById(id: number): Promise<Users> {
    return this.prisma.users.findUnique({ where: { id } });
  }

  async getAgentById(id: number): Promise<Users> {
    return this.prisma.users.findFirst({ where: { id, role: UserType.AGENT } });
  }

  async isCorrectPassword(password: string, passwordHash: string): Promise<boolean> {
    return compare(password, passwordHash);
  }

  async generatePassword(password: string): Promise<string>{
    const salt = await genSalt(12);
    return hash(password, salt);
  }

  /* CRUD */
  async getAll(query: ListQueryDto): Promise<ICrudListResponse<Users>> {
    const totalCount = await this.prisma.users.count();
    const result = await this.prisma.users.findMany({
      skip: ((query.page - 1) * query.limit),
      take: query.limit
    });

    return generateCrudListResponse(result, generationPaginate(totalCount, query.page, query.limit));
  }

  async getOne(id: number): Promise<Users> {
    const result = await this.prisma.users.findFirst({
      where: {id}
    });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async create(data: CreateDto): Promise<Users> {
    try {
      data.password = await this.generatePassword(data.password);
      return await this.prisma.users.create({ data });
    } catch (e) {
      throw e;
    }
  }

  async update(id: number, data: UpdateDto): Promise<Users> {
    try {
      if ('password' in data) {
        data.password = await this.generatePassword(data.password);
      }
      return this.prisma.users.update({
        where: { id },
        data
      });
    } catch (e) {
      throw e;
    }
  }

  delete(id: number): Promise<Users> {
    try {
      return this.prisma.users.delete({
        where: { id }
      });
    } catch (e) {
      throw e;
    }
  }
}
