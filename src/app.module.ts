import { MiddlewareConsumer, Module, RequestMethod } from "@nestjs/common";
import { AppController } from '@/app.controller';
import { AppService } from '@/app.service';
import { ConfigModule } from "@nestjs/config";
import { UserModule } from '@/modules/user/user.module';
import { LeadModule } from '@/modules/lead/lead.module';
import { PrismaModule } from "@/modules/prisma/prisma.module";
import { AuthMiddleware } from "@/middleware/auth.middleware";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true
    }),
    PrismaModule,
    UserModule,
    LeadModule
  ],
  exports: [
    PrismaModule,
    UserModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
