import {ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus} from '@nestjs/common';
import {generateResponse} from "@/helpers/helper-functions";
import { Prisma } from '@prisma/client';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter{
	catch(exception: unknown, host: ArgumentsHost) {
		const ctx = host.switchToHttp();
		const response = ctx.getResponse();

		if (exception instanceof Prisma.PrismaClientKnownRequestError) {
			console.log("PRISMA ERROR: ", exception)
			response.status(HttpStatus.BAD_REQUEST).json(generateResponse(
				null,
				[`DB ERROR ${exception.code}: ${exception.name}`],
				HttpStatus.BAD_REQUEST
			));
		} else {
			const status =
				exception instanceof HttpException
					? exception.getStatus()
					: HttpStatus.INTERNAL_SERVER_ERROR;

			const excRes: any =
				exception instanceof HttpException
					? exception.getResponse()
					: {};

			let messageList: string[];
			if (typeof excRes === 'string') {
				messageList = [excRes];
			} else if (typeof excRes === 'object') {
				if (typeof excRes.message === 'string') {
					messageList = [excRes.message];
				} else if (Array.isArray(excRes.message)) {
					messageList = excRes.message;
				}
			}
			response.status(status).json(generateResponse(
				null,
				messageList,
				status
			));
		}
	}
}
