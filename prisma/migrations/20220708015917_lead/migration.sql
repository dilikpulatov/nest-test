/*
  Warnings:

  - The primary key for the `LeadsOnUsers` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `user_id` on the `LeadsOnUsers` table. All the data in the column will be lost.
  - Added the required column `agent_id` to the `LeadsOnUsers` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "LeadsOnUsers" DROP CONSTRAINT "LeadsOnUsers_user_id_fkey";

-- AlterTable
ALTER TABLE "LeadsOnUsers" DROP CONSTRAINT "LeadsOnUsers_pkey",
DROP COLUMN "user_id",
ADD COLUMN     "agent_id" INTEGER NOT NULL,
ADD CONSTRAINT "LeadsOnUsers_pkey" PRIMARY KEY ("lead_id", "agent_id");

-- AddForeignKey
ALTER TABLE "LeadsOnUsers" ADD CONSTRAINT "LeadsOnUsers_agent_id_fkey" FOREIGN KEY ("agent_id") REFERENCES "Users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
