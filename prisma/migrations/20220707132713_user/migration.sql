-- CreateTable
CREATE TABLE "Users" (
    "id" SERIAL NOT NULL,
    "role" SMALLINT NOT NULL,
    "phone" VARCHAR(20),
    "email" VARCHAR(50),
    "first_name" VARCHAR(50) NOT NULL,
    "last_name" VARCHAR(50) NOT NULL,
    "password" VARCHAR(100),
    "comment" VARCHAR(100),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "update_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Users_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Users_phone_key" ON "Users"("phone");

-- CreateIndex
CREATE UNIQUE INDEX "Users_email_key" ON "Users"("email");

-- CreateIndex
CREATE INDEX "idx_user_email" ON "Users"("email");

-- CreateIndex
CREATE INDEX "idx_user_phone" ON "Users"("phone");
