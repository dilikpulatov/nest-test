-- CreateTable
CREATE TABLE "UsersSession" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "token" VARCHAR(500),

    CONSTRAINT "UsersSession_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "UsersSession_token_key" ON "UsersSession"("token");

-- CreateIndex
CREATE INDEX "idx_user_session_user_id" ON "UsersSession"("user_id");

-- CreateIndex
CREATE INDEX "idx_user_session_token" ON "UsersSession"("token");

-- AddForeignKey
ALTER TABLE "UsersSession" ADD CONSTRAINT "UsersSession_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "Users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
